//https://codepen.io/anon/pen/Jmpcq/
function countdown(element, seconds) {
  var el = document.getElementById(element);
  var max = 30;
  // Set the timer
  var interval = setInterval(function() {
    seconds = seconds < 0 ? max : seconds;
    el.innerHTML =  seconds;
    seconds--;
  }, 1000);
}

//Start as many timers as you want

var timers = document.getElementsByClassName('timer');
countdown('countdown', 3);
